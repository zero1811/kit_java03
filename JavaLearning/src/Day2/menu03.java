package Day2;

import java.util.Scanner;

public class menu03 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("a = ");
        int a = scanner.nextInt();
        System.out.println("b = ");
        int b = scanner.nextInt();
        while (true){

            menu();
            int choose = scanner.nextInt();
            switch (choose){
                case 1:{
                    System.out.println("a : "+ a);
                    System.out.println("b : "+ b);
                    tong(a,b);
                    break;
                }
                case 2:{
                    System.out.println("a : "+ a);
                    System.out.println("b : "+ b);
                    hieu(a,b);
                    break;
                }
                case 3:{
                    System.out.println("a : "+ a);
                    System.out.println("b : "+ b);
                    tich(a,b);
                    break;
                }
                case 4:{
                    System.out.println("a : "+ a);
                    System.out.println("b : "+ b);
                    thuong(a,b);
                    break;
                }
            }
        }
    }

    private static void thuong(int a, int b) {
        float s = a/b;
        System.out.printf("Thương của a và b : %.2f \n",s);
    }

    private static void tich(int a, int b) {
        System.out.println("Tích của a và b là : "+(a*b));
    }

    private static void hieu(int a, int b) {
        System.out.println("Hiệu của a và b : " +(a-b));
    }

    private static void tong(int a, int b) {
        System.out.println("Tổng của a và b : " + (a+b));
    }

    public static void menu(){
        System.out.println("1. Tổng");
        System.out.println("2. Hiệu");
        System.out.println("3. Tích");
        System.out.println("4. Thương");
    }
}
