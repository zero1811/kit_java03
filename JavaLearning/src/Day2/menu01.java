package Day2;

import java.util.Arrays;
import java.util.Scanner;

public class menu01 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int [] listNumber = new int[10];
        scan(listNumber);
        while (true){
            menu();
            int choose = scanner.nextInt();
            switch (choose){
                case 1:{
                    sortList(listNumber);
                    System.out.println();
                    break;
                }
                case 2:{
                    checkNumber(listNumber);
                    System.out.println();
                    break;
                }
                case 3:{
                    System.out.println();
                    printPrime(listNumber);
                    break;
                }
                default:return;
            }
        }
    }

    private static void printPrime(int[] listNumber) {
        for (int i = 0; i < listNumber.length; i++) {
            if (checkPrime(listNumber[i])){
                System.out.print(listNumber[i] +" ");
            }
        }
    }

    private static void checkNumber(int[] listNumber) {
        System.out.println("Nhập số cần kiểm tra : ");
        int number = scanner.nextInt();
        int count = 0;
        for (int i = 0; i < listNumber.length; i++) {
            if (number == listNumber[i])
                count++;
        }
        if (count > 0)
            System.out.println("Số "+number+" có tồn tại trong mảng");
    }

    private static void sortList(int[] listNumber) {
        Arrays.sort(listNumber);
        for (int i = 0; i < listNumber.length; i++) {
            System.out.print(listNumber[i] + " ");
        }
    }

    private static void scan(int[] listNumber) {
        for (int i = 0; i < listNumber.length; i++) {
            System.out.println("A[" + i + "] : ");
            listNumber[i] = scanner.nextInt();
        }
    }
    public static void menu(){
        System.out.println("1. Hiển thị dãy số tăng dần.");
        System.out.println("2. Kiểm tra một số có thuộc mảng hay không.");
        System.out.println("3. Hiển thị danh sách số nguyên tố.");
    }
    public static boolean checkPrime(int n){
        if (n < 2)
            return false;
        else{
            for (int i = 2; i < n-1; i++) {
                if (n % i ==0)
                    return false;
            }
            return true;
        }
    }
}
