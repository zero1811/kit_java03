package Day2;

import java.util.Scanner;

public class menu02 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Nhập chuỗi : ");
        String string = scanner.nextLine();
        while (true){
            menu();
            int chose = scanner.nextInt();
            switch (chose){
                case 1:{
                    checkString(string);
                    System.out.println();
                    break;
                }
                case 2:{
                    checkCharacter(string);
                    System.out.println();
                    break;
                }
                case 3:{
                    ecitCharacter(string);
                    System.out.println();
                    break;
                }
                default:return;
            }
        }
    }

    private static void ecitCharacter(String string) {
        System.out.println("Nhập ký tự ");
        String charac = scanner.next();
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (charac.equalsIgnoreCase(String.valueOf(string.charAt(i)))) {
                count++;
            }
        }
        if (count > 0){
            System.out.println("Nhập ký tự thay thế : ");
            String rep = scanner.next();
            string = string.replaceAll(charac,rep);
        }
        System.out.println("Chuỗi sau khi thay thế : "+string);
    }

    private static void checkCharacter(String string) {
        System.out.println("Nhập ký tự cần kiểm tra : ");
        String charac = scanner.next();
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (charac.equalsIgnoreCase(String.valueOf(string.charAt(i))))
                count++;
        }
        if(count > 0)
            System.out.println("Tồn tại");
        else
            System.out.println("Không tồn tại");
    }

    private static void checkString(String string) {
        System.out.println("Nhập chuỗi cần kiểm tra : ");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        if (string.indexOf(str) != -1){
            System.out.println("Có tồn tại ");
        }
        else
            System.out.println("Không tồn tại");
    }
    public static void menu(){
        System.out.println("1. Kiểm tra chuỗi.");
        System.out.println("2. Kiểm tra ký tự.");
        System.out.println("3. Sửa ký tự.");
    }
}
