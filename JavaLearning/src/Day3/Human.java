package Day3;

import java.util.Calendar;

public class Human {
    private int id;
    private String name;
    private int date;
    private String address;


    public Human() {
    }

    public Human(int id, String name, String data, String address) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int data) {
        this.date = data;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public int getOld() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR)- this.getDate();

    }
}
