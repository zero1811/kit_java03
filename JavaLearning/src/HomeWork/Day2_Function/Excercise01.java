package HomeWork.Day2_Function;

import java.util.Scanner;

public class Excercise01 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Nhập kích thước của mảng : ");
        int size = scanner.nextInt();
        int max = 0;
        int min = 0;
        int [] listNumber = new int[size];
        while (true){
            menu();
            int choose = scanner.nextInt();
            switch (choose){
                case 1:{
                    scan(listNumber);
                    break;
                }
                case 2:{
                    max = getMax(listNumber);
                    min = getMin(listNumber);
                    break;
                }
                case 3:{
                    divece(listNumber);
                    break;
                }
                case 4:{
                    divisorOfMax(max,listNumber);
                    break;
                }
                case 5:{
                    divisorOfMin(min,listNumber);
                    break;
                }
                case 6 :{
                    printEvenNumber(listNumber);
                    break;
                }
                default:return;
            }

        }
    }
    //In số chẵn
    private static void printEvenNumber(int[] listNumber) {
        for (int i = 0; i < listNumber.length; i++) {
            if (listNumber[i] % 2 == 0)
                System.out.print(listNumber[i] + " ");
        }
        System.out.println();
    }
    //In ước của Min
    private static void divisorOfMin(int min, int[] listNumber) {
        System.out.println("Ước của Min : ");
        for (int i = 0; i < listNumber.length; i++) {
            if (listNumber[i] % min == 0)
                System.out.print(listNumber[i] + " ");
        }
        System.out.println();
    }
    //In ước của Max
    private static void divisorOfMax(int max, int[] listNumber) {
        System.out.println("Ước của max : ");
        for (int i = 0; i < listNumber.length; i++) {
            if (listNumber[i] % max == 0)
                System.out.print(listNumber[i] + " ");
        }
        System.out.println();
    }
    //In số chia hết cho 2 và 3
    private static void divece(int[] listNumber) {
        System.out.println("Ước của 2 và 3 : ");
        for (int i = 0; i < listNumber.length; i++) {
            if (listNumber[i] % 2 == 0 && listNumber[i] % 3 == 0)
                System.out.print(listNumber[i] + " ");
        }
        System.out.println();
    }
    //In Min
    private static int getMin(int[] listNumber) {
        int min = listNumber[0];
        for (int i = 0; i < listNumber.length ; i++) {
            if (min > listNumber[i])
                min = listNumber[i];
        }
        System.out.println("Min : " + min);
        return min;
    }
    //In Max
    private static int getMax(int[] listNumber) {
        int max = listNumber[0];
        for (int i = 0; i < listNumber.length; i++) {
            if (max < listNumber[i])
                max = listNumber[i];
        }
        System.out.println("Max : " + max);
        return max;
    }
    //Nhập mảng
    private static void scan(int[] listNumber) {
        for (int i = 0; i < listNumber.length; i++) {
            System.out.println("A["+i+"] : ");
            listNumber[i] = scanner.nextInt();
        }
    }

    public static void menu(){
        System.out.println("1. Nhập mảng ");
        System.out.println("2. Tìm MAX, MIN");
        System.out.println("3. In số nguyên chia hết cho 2 và 3");
        System.out.println("4. In các số là ước của MAX");
        System.out.println("5. In các số là ước của MIN");
        System.out.println("6. In số nguyên chẵn");
    }
}
