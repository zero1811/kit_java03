package HomeWork.Day2_Function;

import java.util.Scanner;

public class Excercise03 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Nhập chuỗi : ");
        String string = scanner.nextLine();
        while (true){
            menu();
            int choose = scanner.nextInt();
            switch (choose){
                case 1:{
                    printFlower(string);
                    break;
                }
                case 2 :{
                    stringToHex(string);
                    break;
                }
                case 3 : {
                    toUpp(string);
                    break;
                }
                default:return;
            }
        }
    }

    public static void stringToHex(String string) {
        int count = 0;
        String tmp = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập ký tự cần tìm : ");
        String charac = scanner.next();
        Character character = charac.charAt(0);
        for (int i = 0; i < string.length(); i++) {
            if (charac.equalsIgnoreCase(String.valueOf(string.charAt(i)))){
                count++;
            }
        }
        if (count == 0)
            System.out.println("Ký tự không tồn tại");
        else {
            String arr [] = string.split(charac);
            for (String x : arr) {
                tmp = tmp +x + Integer.toHexString((int)character);
            }
            System.out.println("Chuỗi đã đổi : " + tmp);
        }
    }

    private static void toUpp(String string) {
        String [] arrString = string.split(" ");
        String tmp = "";
        for (String output: arrString) {
            output = output.substring(0,1).toUpperCase() + output.substring(1);
            tmp = tmp + output + " ";
        }
        System.out.println(tmp);
    }

    private static void printFlower(String string) {
        string = string.toUpperCase();
        System.out.println("Chuỗi in hoa : " + string);
    }
    public static void menu(){
        System.out.println("1. Viết hoa toàn bộ chuỗi");
        System.out.println("2. Đổi ký tự");
        System.out.println("3. Viết hoa chữ cái đầu của từ ");
    }
}
