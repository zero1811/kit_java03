package HomeWork.Day2_Function;

import java.util.Scanner;

public class Excercise02 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Nhập hàng : ");
        int n = scanner.nextInt();
        System.out.println("Nhập cột : ");
        int m = scanner.nextInt();
        int [][] matrix = new int[n][m];
        while (true){
            menu();
            int choose = scanner.nextInt();
            switch (choose){
                case 1 :{
                    scan(matrix);
                    print(matrix);
                    break;
                }
                case 2 : {
                    mainDiagonal(matrix);
                    break;
                }
                case 3:{
                    printPrimeLine(matrix);
                    break;
                }
                case 4:{
                    printSquareNumbers(matrix);
                    break;
                }
                case 5 :{
                    sumLine(matrix);
                    break;
                }
                default:return;
            }
        }
    }

    public static void print(int [][] matrix){
        System.out.println("In mảng : ");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
    private static void scan(int[][] matrix) {
        System.out.println("Nhập mảng : ");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = scanner.nextInt();
            }

        }
    }

    private static void sumLine(int[][] matrix) {
        System.out.println("Nhập hàng cần tính tổng : ");
        int line = scanner.nextInt();
        int sum = 0 ;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == line)
                    sum += matrix[i][j];
            }
        }
        System.out.println("Tổng các số trên hàng là : " + sum);
    }

    public static boolean checkSquareNumber(int n){
        int sqtn = (int) Math.sqrt(n);
        if (sqtn * sqtn == n)
            return true;
        else
            return false;
    }
    private static void printSquareNumbers(int[][] matrix) {
        System.out.println("Nhập cột cần in : ");
        int row = scanner.nextInt();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if( j == row){
                    if (checkSquareNumber(matrix[i][j]))
                        System.out.print(matrix[i][j] + " ");
                }
            }
        }
        System.out.println();
    }

    public static boolean checkPrime(int n){
        if (n < 2)
            return false;
        else {
            for (int i = 2; i < n-1; i++) {
                if (n % i ==0 )
                    return false;
            }
            return true;
        }
    }
    private static void printPrimeLine(int[][] matrix) {
        System.out.println("Nhập hàng cần in : ");
        int line = scanner.nextInt();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == line){
                    if (checkPrime(matrix[line][j]))
                        System.out.print(matrix[i][j] + " ");
                }
            }
        }
        System.out.println();
    }

    private static void mainDiagonal(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == j)
                    System.out.print(matrix[i][j] + " ");
            }
        }
        System.out.println();
    }

    public static void menu(){
        System.out.println("1. Nhập Ma trận");
        System.out.println("2. In đường chéo chính");
        System.out.println("3. In danh sách số nguyên tố trên một hàng");
        System.out.println("4. In danh sách số chính phương trên một cột");
        System.out.println("5. Tính tổng các số trên một hàng");
    }
}
