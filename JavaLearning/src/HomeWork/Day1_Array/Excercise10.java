package HomeWork.Day1_Array;

import java.util.Arrays;
import java.util.Scanner;

public class Excercise10 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int [][] arr = new int[3][3];
        scanArray(arr);
        System.out.println("Mảng vừa nhập : ");
        printArray(arr);

        System.out.println("Mảng sau khi sắp xếp tăng dần : ");
        printArray(arr);
    }
    public static void scanArray(int a[][]){
        System.out.println("Nhập mảng : ");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println("A["+i+"]["+j+"] : ");
                a[i][j] = scanner.nextInt();
            }
        }
    }
    public static void printArray(int a[][]){
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] +" ");
            }
            System.out.println();
        }
    }

}
