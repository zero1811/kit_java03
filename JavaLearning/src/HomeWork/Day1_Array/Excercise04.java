package HomeWork.Day1_Array;

import java.util.Scanner;

public class Excercise04 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        scanArray(arr);
        int count = 0;
        System.out.println("Nhập số nguyên k : ");
        int k = scanner.nextInt();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == k)
                count++;
        }
        if (count == 0)
            System.out.println("Số nguyên "+k+" không xuất hiện trong mảng");
        else
            System.out.println("Số nguyên "+k+" xuất hiên "+count+" lần trong mảng");
    }
    //Nhập mảng
    public static void scanArray(int a[]){
        for (int i = 0; i < a.length; i++) {
            System.out.println("A["+i+"] : ");
            a[i] = scanner.nextInt();
        }
    }
}
