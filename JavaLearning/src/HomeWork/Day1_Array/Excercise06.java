package HomeWork.Day1_Array;

import java.util.Scanner;

public class Excercise06 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        int [] number01 = new int[size];
        int [] number10 = new int[size];
        scanArray(arr);
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0)
                number10[count] = arr[i];
            else
                number01[count] = arr[i];
            count++;
        }
    }
    //Nhập mảng
    public static void scanArray(int a[]){
        for (int i = 0; i < a.length; i++) {
            System.out.println("A["+i+"] : ");
            a[i] = scanner.nextInt();
        }
    }
}
