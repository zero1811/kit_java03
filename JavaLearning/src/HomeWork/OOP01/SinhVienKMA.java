package HomeWork.OOP01;

import java.util.Scanner;

public class SinhVienKMA {
    private String hoTen;
    private String queQuan;
    private String namSinh;
    private String maKhoa;

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    public String getNamSinh() {
        return namSinh;
    }

    public void setNamSinh(String namSinh) {
        this.namSinh = namSinh;
    }

    public String getMaKhoa() {
        return maKhoa;
    }

    public void setMaKhoa(String maKhoa) {
        this.maKhoa = maKhoa;
    }

    public SinhVienKMA() {
    }

    public SinhVienKMA(String hoTen, String queQuan, String namSinh, String maKhoa) {
        this.hoTen = hoTen;
        this.queQuan = queQuan;
        this.namSinh = namSinh;
        this.maKhoa = maKhoa;
    }
    public void nhapSinhVien(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Họ tên : ");
        String ten = scanner.nextLine();
        this.setHoTen(ten);
        System.out.println("Quê quán : ");
        String queQuan = scanner.nextLine();
        this.setQueQuan(queQuan);
        System.out.println("Năm sinh : ");
        String namSinh = scanner.nextLine();
        this.setNamSinh(namSinh);
        System.out.println("Mã khoa : ");
        String maKhoa = scanner.nextLine();
        this.setMaKhoa(maKhoa);
    }
}
