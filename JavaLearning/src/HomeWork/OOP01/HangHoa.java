package HomeWork.OOP01;

import java.util.Scanner;

public class HangHoa {
    private String tenSanPham;
    private int soLuong;
    private int giaNhapVao;
    private int giaBanRa;
    private String ngayNhap;
    private String hanSuDung;

    public HangHoa() {
    }

    public HangHoa(String tenSanPham, int soLuong, int giaNhapVao, int giaBanRa, String ngayNhap, String hanSuDung) {
        this.tenSanPham = tenSanPham;
        this.soLuong = soLuong;
        this.giaNhapVao = giaNhapVao;
        this.giaBanRa = giaBanRa;
        this.ngayNhap = ngayNhap;
        this.hanSuDung = hanSuDung;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getGiaNhapVao() {
        return giaNhapVao;
    }

    public void setGiaNhapVao(int giaNhapVao) {
        this.giaNhapVao = giaNhapVao;
    }

    public int getGiaBanRa() {
        return giaBanRa;
    }

    public void setGiaBanRa(int giaBanRa) {
        this.giaBanRa = giaBanRa;
    }

    public String getNgayNhap() {
        return ngayNhap;
    }

    public void setNgayNhap(String ngayNhap) {
        this.ngayNhap = ngayNhap;
    }

    public String getHanSuDung() {
        return hanSuDung;
    }

    public void setHanSuDung(String hanSuDung) {
        this.hanSuDung = hanSuDung;
    }
    public void nhapHang(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Tên sản phẩm : ");
        String tenSanPham = scanner.nextLine();
        this.setTenSanPham(tenSanPham);
        System.out.println("Số lượng : ");
        int soLuong = scanner.nextInt();
        this.setSoLuong(soLuong);
        System.out.println("Giá nhập vào : ");
        int giaNhap = scanner.nextInt();
        this.setGiaNhapVao(giaNhap);
        System.out.println("Giá bán ra : ");
        int giaBan = scanner.nextInt();
        this.setGiaBanRa(giaBan);
        System.out.println("Ngay nhập : ");
        String ngayNhap = scanner.nextLine();
        this.setNgayNhap(ngayNhap);
        System.out.println("Hạn sử dụng : ");
        String hanSuDung = scanner.nextLine();
        this.setHanSuDung(hanSuDung);
    }

    @Override
    public String toString() {
        return "HangHoa{" +
                "tenSanPham='" + tenSanPham + '\'' +
                ", soLuong=" + soLuong +
                ", giaNhapVao=" + giaNhapVao +
                ", giaBanRa=" + giaBanRa +
                ", ngayNhap='" + ngayNhap + '\'' +
                ", hanSuDung='" + hanSuDung + '\'' +
                '}';
    }
}
