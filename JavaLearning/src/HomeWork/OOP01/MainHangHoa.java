package HomeWork.OOP01;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class MainHangHoa {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HangHoa hangHoa = new HangHoa();
        do {
            System.out.println("1.  Nhập và in thông tin hàng hóa");
            System.out.println("2. In tổng tiền của hàng hóa");
            System.out.println("3. Hạn sử dụng của hàng hóa");
            int choose = scanner.nextInt();
            switch (choose){
                case 1:{
                    nhapVaIn(hangHoa);
                    break;
                }
                case 2:{
                    tongTienHang(hangHoa);
                    break;
                }
                case 3:{
                    hanSuDung(hangHoa);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void nhapVaIn(HangHoa hangHoa){
        System.out.println("Nhập hàng : ");
        hangHoa.nhapHang();
        System.out.println("Mặt hàng vừa nhập");
        hangHoa.toString();
    }
    public static void tongTienHang(HangHoa hangHoa){
        int tongTienNhap = hangHoa.getGiaNhapVao() * hangHoa.getSoLuong();
        int tongTienBan = hangHoa.getGiaBanRa() * hangHoa.getSoLuong();
        System.out.println("Tổng tiền nhập hàng là : " + tongTienNhap);
        System.out.println("Tổng tiền bán là : " + tongTienBan);
        System.out.println("Tiền lãi : " + (tongTienBan - tongTienNhap));
    }
    public static void hanSuDung(HangHoa hangHoa){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date hanSuDung = simpleDateFormat.parse(hangHoa.getHanSuDung());
        Calendar calendar = Calendar.getInstance();
        Date ngayHienTai = calendar.getTime();
        System.out.println(ngayHienTai);
    }
}
