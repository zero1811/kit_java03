package HomeWork.OOP01;

public class MainFraction {
    public static void main(String[] args) {
        Fraction fraction01 = new Fraction(15,6);
        Fraction fraction02 = new Fraction(16,9);
        System.out.println("2 Phân số vừa nhập ");
        fraction01.inPhanSo();
        fraction02.inPhanSo();
        System.out.println("Phân số đã đc rút gọn : ");
        fraction01.rutGon();
        fraction02.rutGon();
        fraction01.inPhanSo();
        fraction02.inPhanSo();
        congHaiPhanSo(fraction01,fraction02);
        truHaiPhanSo(fraction01,fraction02);
        nhanHaiPhanSo(fraction01,fraction02);
        chiaHaiPhanSo(fraction01,fraction02);
        System.out.println("Dạng thập phân của 2 phân số là : ");
        fraction01.inSoThapPhan();
        fraction02.inSoThapPhan();
    }
    public static void congHaiPhanSo(Fraction fraction01,Fraction fraction02){
        int tu = 0;
        int mau = 0;
        if (fraction01.mauSo == fraction02.mauSo){
            tu = fraction01.tuSo + fraction02.tuSo;
            mau = fraction01.mauSo + fraction02.mauSo;
        }
        else {
            mau = fraction01.mauSo * fraction02.mauSo;
            tu = fraction01.tuSo * fraction02.mauSo + fraction02.tuSo * fraction01.mauSo;
        }
        System.out.println(fraction01.inPhanSo() +  " + " + fraction02.inPhanSo() + " = " + tu + "/" + mau);
    }
    public static void truHaiPhanSo(Fraction fraction01,Fraction fraction02){
        int tu = 0;
        int mau = 0;
        if (fraction01.mauSo == fraction02.mauSo){
            tu = fraction01.tuSo - fraction02.tuSo;
            mau = fraction01.mauSo - fraction02.mauSo;
        }
        else {
            mau = fraction01.mauSo * fraction02.mauSo;
            tu = (fraction01.tuSo * fraction02.mauSo) - (fraction02.tuSo * fraction01.mauSo);
        }
        System.out.println(fraction01.inPhanSo() +  "-" + fraction02.inPhanSo() + " = " + tu + "/" + mau);
    }
    public static void nhanHaiPhanSo(Fraction fraction01,Fraction fraction02){
        int tu = fraction01.tuSo * fraction02.tuSo;
        int mau = fraction01.mauSo * fraction02.mauSo;
        System.out.println(fraction01.inPhanSo() +  "*" + fraction02.inPhanSo() + " = " + tu + "/" + mau);
    }
    public static void chiaHaiPhanSo(Fraction fraction01,Fraction fraction02){
        int tu = fraction01.tuSo * fraction02.mauSo;
        int mau = fraction01.mauSo * fraction02.tuSo;
        System.out.println(fraction01.inPhanSo() +  "*" + fraction02.inPhanSo() + " = " + tu + "/" + mau);
    }

}
