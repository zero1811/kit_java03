package HomeWork.OOP01;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class MainSinhVien {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập một sinh viên : ");
        SinhVienKMA sinhVien = new SinhVienKMA();
        sinhVien.nhapSinhVien();
        do {
            menu();
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    kiemTra20tuoi(sinhVien);
                    break;
                }
                case 2:{
                    if (kiemTraGioiTinh(sinhVien).equalsIgnoreCase("Nữ"))
                        System.out.println("Sinh viên nữ");
                    else
                        System.out.println("Sinh viên nam");
                    break;
                }
                case 3:{
                    kiemTraKhoa(sinhVien);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void menu(){
        System.out.println("1. Kiểm tra sinh viên đó đã đủ 20+");
        System.out.println("2. Kiểm tra sinh viên đó có là nữ");
        System.out.println("3. Kiểm tra sinh viên đó học khoa nào");
    }
    public static void kiemTra20tuoi(SinhVienKMA sinhVien){
        Calendar calendar = Calendar.getInstance();
        int namHienTai = calendar.get(Calendar.YEAR);
        if (namHienTai - Integer.parseInt(sinhVien.getNamSinh()) >= 0)
            System.out.println("Sinh viên đủ 20+");
        else
            System.out.println("Sinh viên chưa đủ 20+");
    }
    public static String kiemTraGioiTinh(SinhVienKMA sinhVien){
        String[] hoTen = sinhVien.getHoTen().split(" ");
        for (String ten: hoTen) {
            if (ten.equalsIgnoreCase("Thị")){
                return "Nữ";
            }
        }
        return "Nam";
    }
    public static void kiemTraKhoa(SinhVienKMA sinhVien){
        if (sinhVien.getMaKhoa().equalsIgnoreCase("AT"))
            System.out.println("Khoa An Toàn Thông Tin");
        else if (sinhVien.getMaKhoa().equalsIgnoreCase("CT"))
            System.out.println("Khoa Công Nghệ Thông Tin");
        else System.out.println("Khoa Điện Tử Viễn Thông");
    }
}
