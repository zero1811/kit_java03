package HomeWork.OOP01;

public class Fraction {
    public int tuSo;
    public int mauSo;

    public Fraction(int tuSo, int mauSo) {
        this.tuSo = tuSo;
        this.mauSo = mauSo;
    }

    public String inPhanSo(){
        return this.tuSo + " / " + this.mauSo;
    }

    public void rutGon(){
        this.tuSo /=ucln(this.tuSo,this.mauSo);
        this.mauSo /= ucln(this.tuSo,this.mauSo);
    }
    public  int ucln(int a,int b){
        int tu = this.tuSo;
        int mau = this.mauSo;
        while (tu != mau) {
            if (tu > mau)
                tu -= mau;
            else
                mau -= tu;
        }
        return tu;
    }
    public void inSoThapPhan(){
        double thapPhan = (float)this.tuSo/this.mauSo;
        System.out.println("Tử số : " + this.tuSo);
        System.out.println("Mẫu số : " + this.mauSo);
        System.out.println("Dạng số thập phân : " + thapPhan);
    }
}
